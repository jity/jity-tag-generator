<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Formater;

/**
 * SimpleFormater
 *
 * Strip all non-word chars from the text, except the '-'.
 *
 * @uses   FormaterInferface
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class SimpleFormater implements FormaterInterface
{
    /**
     * process
     *
     * Run formater over specified text.
     *
     * @param string $text Text to format. (eg. strip non-word chars)
     *
     * @access public
     * @return string
     */
    public function process($text)
    {
        if (false === is_string($text)) {

            throw new \Exception(
                __CLASS__ . '->process(string) :: Param needs to be a string'
            );

            return $text;
        }

        // Remove everything except word characters and '-'
        $text = preg_replace('/[^a-zA-ZäöüÄÖÜß0-9-]/u', ' ', $text);

        // Remove multiple whitespaces
        $text = preg_replace('/\s{2,}/', ' ', $text);

        // Remove trailing and heading spaces
        $text = trim($text);

        return $text;
    }
}

