<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Formater;

/**
 * FormaterInterface
 *
 * Used by the TagGenerator to format the text before a splitter and
 * specified filters can be applied to the text.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
interface FormaterInterface
{
    /**
     * process
     *
     * Run formater over specified text.
     *
     * @param string $text Text to format. (eg. strip non-word chars)
     *
     * @access public
     * @return string
     */
    public function process($text);
}

