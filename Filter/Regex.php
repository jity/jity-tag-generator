<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Filter;

use Jity\TagGeneratorBundle\Entity\TagCollection;

/**
 * Regex
 *
 * Score or remove tags which match a specified regex pattern.
 * This filter requires special options.
 *
 * <code>
 *   array(
 *       'pattern'     => '[^0-9]', // Tags which does not contain numbers
 *       'modificator' => 'i',      // PHP's regex modificators
 *                                  // (i = case-insensitive)
 *   )
 * </code>
 *
 * @uses   AbstractFilter
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Regex extends AbstractFilter
{
    /**
     * process
     *
     * Run filter logic over given TagCollection.
     *
     * @param TagCollection $collection TagCollection to process
     *
     * @access public
     * @return TagCollection
     */
    public function process(TagCollection $collection)
    {
        $pattern = (array_key_exists('pattern', $this->options))
            ? $this->options['pattern']
            : '';

        $modificator = (array_key_exists('modificator', $this->options))
            ? $this->options['modificator']
            : '';

        // Walk through all tags
        foreach ($collection as &$tag) {

            // First char is not uppercase
            if (0 === preg_match(
                '/' . $pattern . '/' . $modificator,
                $tag->getName())) {

                if (true === $this->remove) {

                    // Remove the current element
                    $collection->removeElement($tag);
                }

                continue;
            }

            $tag->setScore(
                $tag->getScore() * $this->factor
            );
        }

        return $collection;
    }
}

