<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Filter;

use Jity\TagGeneratorBundle\Entity\TagCollection;

/**
 * Camelcase
 *
 * Score or remove tags which seems to be camelcased.
 * This filter does not require special options.
 *
 * @uses   FilterInterface
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Camelcase extends AbstractFilter
{
    /**
     * process
     *
     * Run filter logic over given TagCollection.
     *
     * @param TagCollection $collection TagCollection to process
     *
     * @access public
     * @return TagCollection
     */
    public function process(TagCollection $collection)
    {
        // Walk through all tags
        foreach ($collection as &$tag) {

            // First char is not uppercase
            if (0 === preg_match(
                "/[A-Za-z]([A-Z0-9]*[a-z][a-z0-9]*[A-Z]|[a-z0-9]*[A-Z][A-Z0-9]*[a-z])[A-Za-z0-9]*/",
                $tag->getName())) {

                if (true === $this->remove) {

                    // Remove the current element
                    $collection->removeElement($tag);
                }

                continue;
            }

            $tag->setScore(
                $tag->getScore() * $this->factor
            );
        }

        return $collection;
    }
}

