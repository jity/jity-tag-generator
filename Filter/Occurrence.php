<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Filter;

use Jity\TagGeneratorBundle\Entity\TagCollection;

/**
 * Occurrence
 *
 * Score or remove tags which occurrence a (un)specified amount.
 * This filter requires special options.
 *
 * <code>
 *   array(
 *       'min' => 2,  // Tags have a occurrence of min 2
 *       'max' => 10, // Tags have a occurrence of max 10
 *   )
 * </code>
 *
 * @uses   AbstractFilter
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Occurrence extends AbstractFilter
{
    /**
     * process
     *
     * Run filter logic over given TagCollection.
     *
     * @param TagCollection $collection TagCollection to process
     *
     * @access public
     * @return TagCollection
     */
    public function process(TagCollection $collection)
    {
        $min  = (array_key_exists('min', $this->options))
            ? $this->options['min']
            : false;

        $max  = (array_key_exists('max', $this->options))
            ? $this->options['max']
            : false;

        // Walk through all tags
        foreach ($collection as &$tag) {

            $skip = false;

            if ($tag->getOccurrence() < $min && false !== $min) {
                $skip = true;
            }

            if ($tag->getOccurrence() > $max && false !== $max) {
                $skip = true;
            }

            if (true === $skip) {

                if (true === $this->remove) {

                    // Remove the current element
                    $collection->removeElement($tag);
                }

                continue;
            }

            $tag->setScore(
                $tag->getScore() + $tag->getOccurrence() * $this->factor
            );
        }

        return $collection;
    }
}

