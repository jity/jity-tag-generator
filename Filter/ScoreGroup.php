<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Filter;

use Jity\TagGeneratorBundle\Entity\TagCollection;

/**
 * ScoreGroup
 *
 * Remove tags which does not be in the specified range of tag groups.
 * This filter requires special options.
 *
 * <code>
 *   array(
 *       'max' => 2, // Only the first 2 tag groups (grouped by score)
 *   )
 * </code>
 *
 * @uses   AbstractFilter
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class ScoreGroup extends AbstractFilter
{
    /**
     * process
     *
     * Run filter logic over given TagCollection.
     *
     * @param TagCollection $collection TagCollection to process
     *
     * @access public
     * @return TagCollection
     */
    public function process(TagCollection $collection)
    {
        $max = (array_key_exists('max', $this->options))
            ? $this->options['max']
            : false;

        // Nothing to do
        if (false === $this->remove || false === $max) {
            return $collection;
        }

        $count  = 0;
        $groups = array();

        // Walk through all tags
        foreach ($collection as &$tag) {

            // Build groups
            $groups[$tag->getScore()][] = $tag;

            // Clear whole Collection, for refilling
            $collection->removeElement($tag);
        }

        krsort($groups);

        // Walk through all groups
        foreach ($groups as $score => $tags) {

            if ($count >= $max) {
                break;
            }

            foreach ($tags as $tag) {
                $collection->add($tag);
            }

            $count++;
        }

        return $collection;
    }
}

