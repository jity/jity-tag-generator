<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Filter;

use Jity\TagGeneratorBundle\Entity\TagCollection;

/**
 * Length
 *
 * Score or remove tags which fit the specified length.
 * This filter requires special options.
 *
 * <code>
 *   array(
 *       'min' => 2,  // Tags have to have min 2 chars
 *       'max' => 10, // Tags have to have max 10 chars
 *   )
 * </code>
 *
 * @uses AbstractFilter
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Length extends AbstractFilter
{
    /**
     * process
     *
     * Run filter logic over given TagCollection.
     *
     * @param TagCollection $collection TagCollection to process
     *
     * @access public
     * @return TagCollection
     */
    public function process(TagCollection $collection)
    {
        $min  = (array_key_exists('min', $this->options))
            ? $this->options['min']
            : false;

        $max  = (array_key_exists('max', $this->options))
            ? $this->options['max']
            : false;

        // Walk through all tags
        foreach ($collection as &$tag) {

            $skip = false;

            if (strlen($tag->getName()) < $min && false !== $min) {
                $skip = true;
            }

            if (strlen($tag->getName()) > $max && false !== $max) {
                $skip = true;
            }

            if (true === $skip) {

                if (true === $this->remove) {

                    // Remove the current element
                    $collection->removeElement($tag);
                }

                continue;
            }

            $tag->setScore(
                $tag->getScore() * $this->factor
            );
        }

        return $collection;
    }
}

