<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Filter;

use Jity\TagGeneratorBundle\Entity\TagCollection;

/**
 * Dictionary
 *
 * Score or remove tags which match (or not) the specified dictionaries.
 * This filter requires special options.
 *
 * <code>
 *   array(
 *       'match'         => true,  // Tags should match or match not dicts
 *       'casesensitive' => false, // Match case-(in)sensitive
 *       'dictionaries'  => array( // Specify dictionaries
 *           'german'    => array( // Language => concrete dictionaries
 *               'adjektive',
 *               'verben',
 *               'klein',
 *               'fixwords'
 *           )
 *       )
 *   )
 * </code>
 *
 * @uses   AbstractFilter
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Dictionary extends AbstractFilter
{
    private $_dictionaries;

    /**
     * __construct
     *
     * Initializes a the filter.
     *
     * @param int   $factor  Score multiplier factor
     * @param mixed $remove  Remove non-matching tags from collection
     * @param array $options Specific configuration for the filter
     *
     * @access public
     * @return void
     */
    public function __construct(
        $factor = 2,
        $remove = false,
        array $options = array())
    {
        parent::__construct($factor, $remove, $options);

        $this->configure();
    }

    /**
     * configure
     *
     * Setup the filter by processing the specified options.
     *
     * @access public
     * @return void
     */
    public function configure()
    {
        $dictionaries = (array_key_exists('dictionaries', $this->options))
            ? $this->options['dictionaries']
            : array();

        foreach ($dictionaries as $language => $dictionaries) {

            // Build dictionary path
            $basePath = __DIR__
                      . '/../Resources/dictionaries/'
                      . $language . '/';

            foreach ($dictionaries as $dictionary) {

                $path = $basePath . $dictionary . '.php';

                // Check if specified dictionary exists
                if (false === file_exists($path)) {

                    throw new \Exception(
                        'Dictionary "' . $dictionary . '" was not found.'
                    );
                    continue;
                }

                // Load compiled dictionary
                eval(
                    '$compiledDictionary = '
                    . file_get_contents($path) . ';'
                );

                // Simply merge dictionaries
                foreach ($compiledDictionary as $word) {
                    $this->_dictionaries[] = $word;
                }
            }
        }
    }

    /**
     * process
     *
     * Run filter logic over given TagCollection.
     *
     * @param TagCollection $collection TagCollection to process
     *
     * @access public
     * @return TagCollection
     */
    public function process(TagCollection $collection)
    {
        $match = (array_key_exists('match', $this->options))
            ? $this->options['match']
            : false;

        $modificator = (array_key_exists('casesensitive', $this->options))
            ? ($this->options['casesensitive']) ? '' : 'i'
            : 'i';

        // Walk through all tags
        foreach ($collection as &$tag) {

            $inList = (preg_grep('/^' . $tag->getName() . '$/' . $modificator, $this->_dictionaries))
                ? true
                : false;

            // Hard Filter
            if ($this->remove) {

                if ($match === $inList) {

                    // Remove the current element
                    $collection->removeElement($tag);
                }

                continue;
            }

            // Soft Filter
            if ($match === $inList) {

                $tag->setScore(
                    $tag->getScore() * $this->factor
                );
            }
        }

        return $collection;
    }
}

