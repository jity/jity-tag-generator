<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Filter;

use Jity\TagGeneratorBundle\Entity\TagCollection;

/**
 * Lowercase
 *
 * Score or remove tags which seems to be all lowercased.
 * This filter does not require special options.
 *
 * @uses   FilterInterface
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Lowercase extends AbstractFilter
{
    /**
     * process
     *
     * Run filter logic over given TagCollection.
     *
     * @param TagCollection $collection TagCollection to process
     *
     * @access public
     * @return TagCollection
     */
    public function process(TagCollection $collection)
    {
        // Walk through all tags
        foreach ($collection as &$tag) {

            // First char is not uppercase
            if (0 === preg_match("/^[a-z]+$/", $tag->getName())) {

                if (true === $this->remove) {

                    // Remove the current element
                    $collection->removeElement($tag);
                }

                continue;
            }

            $tag->setScore(
                $tag->getScore() * $this->factor
            );
        }

        return $collection;
    }
}

