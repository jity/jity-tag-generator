<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Filter;

use Jity\TagGeneratorBundle\Entity\TagCollection;

/**
 * AbstractFilter
 *
 * A filter can score tags with a specific logic eg. the matching of a
 * tag over a given length. A filter can also be used to strip unmatching
 * tags from the collection by a specific logic.
 *
 * @uses   FilterInterface
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
abstract class AbstractFilter implements FilterInterface
{
    protected $factor;
    protected $remove;
    protected $options;

    /**
     * __construct
     *
     * Initializes a abstract filter.
     *
     * @param int   $factor  Score multiplier factor
     * @param mixed $remove  Remove non-matching tags from collection
     * @param array $options Specific configuration for the filter
     *
     * @access public
     * @return void
     */
    public function __construct(
        $factor = 2,
        $remove = false,
        array $options = array())
    {
        $this->factor  = $factor;
        $this->remove  = $remove;
        $this->options = $options;
    }

    /**
     * process
     *
     * Run filter logic over given TagCollection.
     *
     * @param TagCollection $collection TagCollection to process
     *
     * @abstract
     * @access public
     * @return TagCollection
     */
    abstract public function process(TagCollection $collection);
}

