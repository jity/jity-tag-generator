<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Entity;

/**
 * Tag
 *
 * The most basic unit of text-to-tag processing.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class Tag
{
    private $name;
    private $occurrence;
    private $score;

    /**
     * __construct
     *
     * Initializes a new tag.
     *
     * @param string $name Name of the tag
     *
     * @access public
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * getName
     *
     * Receive the name of the tag.
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * setName
     *
     * Set the name of the tag.
     *
     * @param string $name Name of the tag
     *
     * @access public
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * getOccurency
     *
     * Receive the occurrence of the tag.
     *
     * @access public
     * @return integer
     */
    public function getOccurrence()
    {
        return $this->occurrence;
    }

    /**
     * setOccurency
     *
     * Set the occurrence of the tag.
     *
     * @param integer $occurrence Occurrence of the tag
     *
     * @access public
     * @return Tag
     */
    public function setOccurrence($occurrence)
    {
        $this->occurrence = $occurrence;

        return $this;
    }

    /**
     * getScore
     *
     * Receive the current score of the tag.
     *
     * @access public
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * setScore
     *
     * Set the current score of the tag.
     *
     * @param integer $score Score to set
     *
     * @access public
     * @return Tag
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }
}

