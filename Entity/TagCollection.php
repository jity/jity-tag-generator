<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * TagCollection
 *
 * Collection which holds all tags to a given source text.
 *
 * @uses   ArrayCollection
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class TagCollection extends ArrayCollection
{
    private $source;

    /**
     * __construct
     *
     * Initializes a new TagCollection.
     *
     * @param array $tokens Tokens processed by a splitter
     *
     * @access public
     * @return void
     */
    public function __construct(array $tokens)
    {
        foreach ($tokens as $name => $occurrence) {

            $tag = new Tag($name);

            $tag->setOccurrence($occurrence);
            $tag->setScore(1);
            $this->add($tag);
        }
    }

    /**
     * setSource
     *
     * Set the source text to the collection.
     *
     * @param string $source Text to set as source
     *
     * @access public
     * @return TagCollection
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * getSource
     *
     * Receive the text source of the collection.
     *
     * @access public
     * @return text
     */
    public function getSource()
    {
        return $this->source;
    }
}

