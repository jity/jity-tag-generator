<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Splitter;

/**
 * SplitterInterface
 *
 * Used by the TagGenerator to split the formated text into tokens
 * so they can be procced by specified filters.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
interface SplitterInterface
{
    /**
     * process
     *
     * Run splitter over specified text.
     *
     * @param string $text Text to split into tokens.
     *
     * @access public
     * @return array
     */
    public function process($text);
}

