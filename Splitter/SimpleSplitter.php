<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Splitter;

/**
 * SimpleSplitter
 *
 * Simply split the text with explode and count occurrence.
 *
 * @uses   SplitterInterface
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class SimpleSplitter implements SplitterInterface
{
    /**
     * process
     *
     * Run splitter over specified text.
     *
     * @param string $text Text to split into tokens.
     *
     * @access public
     * @return array
     */
    public function process($text)
    {
        if (false === is_string($text)) {

            throw new \Exception(
                __CLASS__ . '->process(string) :: Param needs to be a string'
            );

            return $text;
        }

        $words   = explode(' ', $text);
        $toplist = array();

        foreach ($words as $word) {

            if (false === array_key_exists($word, $toplist)) {

                // We found a new word, lets mark the occurrence
                $toplist[$word] = 1;

            } else {

                // We found a known word, just count up one more occurrence
                $toplist[$word]++;
            }
        }

        // Sort the array (low -> high)
        asort($toplist);

        return $toplist;
    }
}

