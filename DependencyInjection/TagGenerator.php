<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\DependencyInjection;

use Jity\TagGeneratorBundle\Entity\TagCollection,
    Jity\TagGeneratorBundle\Formater\FormaterInterface,
    Jity\TagGeneratorBundle\Splitter\SplitterInterface,
    Jity\TagGeneratorBundle\Filter\FilterInterface,
    Jity\TagGeneratorBundle\Formater\SimpleFormater,
    Jity\TagGeneratorBundle\Splitter\SimpleSplitter,
    Jity\TagGeneratorBundle\Filter\Score,
    Jity\TagGeneratorBundle\Filter\ScoreGroup,
    Jity\TagGeneratorBundle\Filter\Length,
    Jity\TagGeneratorBundle\Filter\Occurrence,
    Jity\TagGeneratorBundle\Filter\Dictionary,
    Jity\TagGeneratorBundle\Filter\Capitalized,
    Jity\TagGeneratorBundle\Filter\Uppercase,
    Jity\TagGeneratorBundle\Filter\Camelcase,
    Jity\TagGeneratorBundle\Filter\Regex;

/**
 * TagGenerator
 *
 * Central controll unit of the text-to-tag processing.
 * With the help of this generator you be able to transform
 * any text to a usefull collection of tags.
 *
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class TagGenerator
{
    private $_formater;
    private $_splitter;
    private $_filter;

    /**
     * __construct
     *
     * Initializes a new TagGenerator.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->_formater = new SimpleFormater();
        $this->_splitter = new SimpleSplitter();
    }

    /**
     * setFormater
     *
     * Specify the Formater which will be used.
     *
     * @param FormaterInterface $formater Formater to use
     *
     * @access public
     * @return void
     */
    public function setFormater(FormaterInterface $formater)
    {
        $this->_formater = $formater;

        return $this;
    }

    /**
     * setSplitter
     *
     * Specify the Splitter which will be used.
     *
     * @param SplitterInterface $splitter Splitter to use
     *
     * @access public
     * @return void
     */
    public function setSplitter(SplitterInterface $splitter)
    {
        $this->_splitter = $splitter;

        return $this;
    }

    /**
     * addFilter
     *
     * Put a new filter in the processing chain.
     *
     * @param FilterInterface $filter Filter to chain in
     *
     * @access public
     * @return void
     */
    public function addFilter(FilterInterface $filter)
    {
        $this->_filter[] = $filter;

        return $this;
    }

    /**
     * getTags
     *
     * Process the specified text to tags.
     *
     * @param string $text Text to process
     *
     * @access public
     * @return TagCollection
     */
    public function getTags($text)
    {
        // Backup source text
        $source = $text;

        // Just cleanup the text with the specified Formater
        $text = $this->_formater->process($text);

        // Split the text into tokens with the specified Splitter
        $tokens = $this->_splitter->process($text);

        // Add the tokens to a new Tag Collection
        $collection = new TagCollection($tokens);

        // Save the backuped text as source inside of the collection
        $collection->setSource($source);

        return $this->filter($collection);
    }

    /**
     * filter
     *
     * Run all configured filters of the generator
     * over the specified TagCollection.
     *
     * @param TagCollection $collection TagCollection to process
     *
     * @access public
     * @return TagCollection
     */
    public function filter(TagCollection $collection)
    {
        // No filters registered, init default filters
        if (true === empty($this->_filter)) {
            $this->initDefaultFilter();
        }

        // Process all Filters on the Collection
        foreach ($this->_filter as $filter) {
            $collection = $filter->process($collection);
        }

        return $collection;
    }

    /**
     * initDefaultFilter
     *
     * @access private
     * @return void
     */
    private function initDefaultFilter()
    {
        // Configure all Filters
        $this

            /* Remove words shorter than 3 chars */
            ->addFilter(
                new Length(1, true, array(
                    'min' => 2
                ))
            )

            /* Remove most useless words from collection */
            ->addFilter(
                new Dictionary(1, true, array(
                    'match'         => true,
                    'casesensitive' => false,
                    'dictionaries'  => array(
                        'german'    => array(
                            'adjektive',
                            'verben',
                            'klein',
                            'fixwords',
                            'stopwords'
                        )
                    )
                ))
            )

            /* Score words ackknowledged by our dictionary */
            ->addFilter(
                new Dictionary(5, false, array(
                    'match'         => true,
                    'casesensitive' => false,
                    'dictionaries'  => array(
                        'german'    => array(
                            'worte'
                        )
                    )
                ))
            )

            /* Score occurrence of remaining words */
            ->addFilter(
                new Occurrence(5)
            )

            /* Score uppercased words */
            ->addFilter(new Uppercase(15))

            /* Score camelcased words */
            ->addFilter(new Camelcase(15))

            /* Score capitalized words */
            ->addFilter(new Capitalized(5))

            ->addFilter(
                new Regex(2, false, array(
                    'pattern' => '[-]+'
                ))
            )

            /* Score words which are not ackknowledged by our dictionary */
            ->addFilter(
                new Dictionary(10, false, array(
                    'match'         => false,
                    'casesensitive' => false,
                    'dictionaries'  => array(
                        'german'    => array(
                            'worte'
                        )
                    )
                ))
            )

            /* Remove words which does not fit the range */
            ->addFilter(
                new Score(1, true, array(
                    'min' => 300
                ))
            )

            /* Remove words by score-groups */
            ->addFilter(
                new ScoreGroup(1, true, array(
                    'max' => 3
                ))
            );
    }
}

