<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder,
    Symfony\Component\Config\FileLocator,
    Symfony\Component\HttpKernel\DependencyInjection\Extension,
    Symfony\Component\DependencyInjection\Loader;

/**
 * JityTagGeneratorExtension
 *
 * @uses   Extension
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class JityTagGeneratorExtension extends Extension
{
    /**
     * load
     *
     * @param array            $configs   Configuration Tree
     * @param ContainerBuilder $container Dependency Injection Container
     *
     * @access public
     * @return void
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');
    }
}

