#!/bin/bash

raw="`pwd`/$1-raw.php"
compiled="`pwd`/$1.php"

echo -e "<?php\n\$a = array();" > $raw

cat - \
| sed 's/\/.*//g' \
| sed 's/a"/ä/g' \
| sed 's/o"/ö/g' \
| sed 's/u"/ü/g' \
| sed 's/A"/Ä/g' \
| sed 's/O"/Ö/g' \
| sed 's/U"/Ü/g' \
| sed 's/sS/ß/g' \
| sed 's/^/\$a[] = "/g' \
| sed 's/$/";/g' >> $raw

echo "var_export(\$a);" >> $raw

php $raw > $compiled

rm -f $raw

