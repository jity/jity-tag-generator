<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests;

/**
 * JityTestCase
 *
 * Base testcase class for all Doctrine testcases.
 *
 * @abstract
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
abstract class JityTestCase extends \PHPUnit_Framework_TestCase
{
}

