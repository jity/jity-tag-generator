<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Fixture;

use Jity\TagGeneratorBundle\Entity\TagCollection,
    Jity\TagGeneratorBundle\Entity\Tag;

/**
 * PreparedTagCollection
 *
 * @uses   TagCollection
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class PreparedTagCollection extends TagCollection
{
    /**
     * __construct
     *
     * Initializes a new PreparedTagCollection.
     *
     * @param boolean $randScores Switch to (en|dis)able random scored tags
     *
     * @access public
     * @return void
     */
    public function __construct($randScores = false)
    {
        $words = array_flip(
            file(__DIR__ . '/Resources/words.txt', FILE_IGNORE_NEW_LINES)
        );

        if (true === $randScores) {

            foreach ($words as $name => $occurrence) {

                $tag = new Tag($name);
                $tag->setOccurrence($occurrence);
                $tag->setScore(mt_rand(2, 15));
                $this->add($tag);
            }

        } else {

            parent::__construct($words);
        }
    }
}

