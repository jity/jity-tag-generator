Wer kennt das nicht - größere Monitoren bringen größere Auflösungen mit sich, und wer scrollt
dann schon gerne mit nur 3 Zeilen pro Anschlag? Ab der kommenden stabilen Firefox Version 17,
ändern sich die Einstellungen zum Mausrad, zumindest für die, die sie gesetzt haben.

Um ein komfortableres Surfen zu gewährleisten lässt sich das Mausrad, unabhängig vom System,
in Firefox konfigurieren. Gerade das macht sich auf hochauflösenden Monitoren gut, sodass man
schneller die Seiten überfliegen und dadurch, um einiges schneller die gesuchten Informationen
finden kann. Wer diese versteckten Einstellungen vorgenommen hat, dem wird die verminderte
Geschwindigkeit, die mit dem Update auf Firefox 17 kamen, sofort auffallen. Genauso erging es
mir. Mit der Gewissheit einen Bug gefunden zu haben, hat es einige Zeit gebraucht, um
den wahren Grund [^1] für das vermeintliche Fehlverhalten zu finden.

###Firefox 16 oder älter

Unter Firefox 16 oder älter wurden die Einstellungen zum Mausrad mittels der **about:config**
Seite vorgenommen. Wem dies noch neu ist: um auf die versteckten Einstellungen zu gelangen,
wird einfach in die Adressleiste „about:config“ eingegeben. Nach bestätigtem Risikobewusstsein
gelangt man zu einer sehr langen Tabelle, die sich am Kopf durchsuchen lässt. Um das Mausrad
nun zu konfigurieren, müssen folgende Schlüssel-Werte Paare gesetzt werden.

<pre class="prettyprintlang-php linenums"><code>mousewheel.withnokey.sysnumlines => false
mousewheel.withnokey.numlines => 8
</code></pre>

Das erste Paar weist Firefox an, statt der Systemkonfiguration, die Einstellung (Anzahl der
zu scrollenden Zeilen pro Anschlag), welche das zweite Paar bereitstellt, zu nutzen.

###Firefox 17 und neuer

Auch unter Firefox 17 und neuer werden die Einstellungen zum Mausrad unter der **about:config**
Seite vorgenommen. Dabei haben sich jedoch die Schlüssel-Werte Paare
geändert. Die neue Version von Firefox kommt somit mit nur einem Paar aus.

<pre class="prettyprintlang-php linenums"><code>mousewheel.default.delta_multiplier_y => 800</code></pre>

Bei dem Schlüssel handelt es sich um einen Multiplikator, der auf die Weite des
Mausrad Anschlages angewendet wird. Der Wert 800 sollte somit  den
vorher beschriebenen 8 Zeilen entsprechen.

###Ausblick

Ich habe bereits eine kleine [Wissensdatenbank](http://projects.hermann-mayer.net/projects/tutorials-howto-s-und-informationen/wiki/Firefox)
zu Firefox, in einem meiner Projekt-Wikis ins Leben gerufen. Dort finden sich unter anderem
auch Anpassungen an die Benutzeroberfläche von Firefox in dunklen Umgebungen und diverse
Einstellungen um die Leistung des Browsers zu verbessern.

[^1]: [Artikel im Mozilla Wiki](https://wiki.mozilla.org/Gecko:Mouse_Wheel_Scrolling#Preferences_for_customizing_delta_values_and_default_action)

*[Bug]:     Ein Fehler in einem Programm
