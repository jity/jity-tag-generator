Schnell, sauber und effizient soll es sein &mdash; wer stundenlang am Tag in der Shell
arbeitet braucht Unterstützung. Shortcuts, farbliche Untermalung, und einen schnellen
Überblick über Informationen des System oder zum aktuellen Arbeitsverzeichnis. Und
sollte es Jemanden geben, der auch gerne zum entwickeln in der Shell bleiben möchte,
dem wird hiermit ein Ausblick auf Vim als IDE gewährt.

###Bash die Power-Shell

In einem [Projekt](https://gitorious.hermann-mayer.net/linux-environment/vim-config) habe ich
angefangen, Mitte August 2012, meine, für die Entwickelung
und den täglichen Umang, relevanten Linux Profil Einstellungen zu sammeln. Dabei lag das
Hauptaugenmerk anfänglich auf der **Bash RC**, in der die Konfiguration der bekannten Linux
und Unix Shell beheimatet ist. Jahrelang habe ich verschiedene nützliche Einstellungen festgelegt,
wie die Farbgebung von Dateinamen je nach Typ, die automatische Integration der
Automatischen Vervollständigung für diverse Programme wie *tar, git, svn oder man* und noch
vieles mehr.

<img class="fb-img" src="http://cdn.hermann-mayer.net/jity/blog/3/shell_1.png" />

So sieht der Terminal nach dem Startvorgang aus &mdash; alle wichtigen
Informationen gebündelt in einer tabellarischen und damit übersichtlichen Anordnung.

<img class="fb-img" src="http://cdn.hermann-mayer.net/jity/blog/3/shell_6.png" />

####The Joy of Usability

Eine farbliche Markierung der Treffer die *grep* liefert, oder das Auszeichnen von 
Schlüsselwörtern in einer *man-Page* erhöhen erheblich die Lesbarkeit und damit die Effizienz,
die sich aus der schnelleren Nutzbarmachung der Informationen ergibt. Des Weiteren ist das
zurückspringen in einem Pfad ein müßiges Unterfangen, wenn man bei jedem Aufruf das
Programm *cd* dem eigentlichen zwei Punkten, voranstellen muss. Viel schöner ist an dieser
Stelle das einfache tippen von „..“ (zwei Punkten) oder „...“ (drei Punkten), um weiter zurück
im Pfad zu springen. Pager wie *less* oder *more* sollten im Normalfall auch eingefärbte
Passagen in Texten und nicht wilde ASCII Steuerzeichen ausgeben. Neue Pfade sollte man einfach
so mit *mkdir*, ohne Switches wie „-p“, anlegen können. Alle diese Features habe ich,
in diesem  Projekt, zusammengetragen.

<img class="fb-img" src="http://cdn.hermann-mayer.net/jity/blog/3/shell_3.png" />

Dazu kommen unzählige Feinheiten, wie die Automatische Vervollständigung, die unabhängig
von Groß- und Kleinschreibung arbeitet oder der einfache Tab Anschlag, um eine Liste
der zur Verfügung stehenden Optionen zu erhalten.
Mit viel Liebe für‘s Detail ist die „Command Prompt“ entstanden, denn gerade hier sollte
man sich selbst kein Bein stellen, mit zu langen oder unhandlichen Angaben zum aktuellen Pfad.
Aus diesem Grund, habe ich dieses essentielle Element, das ständig wiederholt angezeigt wird, in
zwei Zeilen aufgeteilt. Somit lassen sich wichtige Informationen, wie der Exit-Code des letzten
Befehls, die Anzahl der Dateien und deren Größe - und der Pfad zum aktuellen Arbeitsverzeichnis,
anzeigen. In der zweiten Zeile findet sich dann, eine kurze und recht statische Angabe des aktuellen
Benutzer und der Name der Maschine auf der man gerade eingeloggt ist. Der Vorteil einer
zweiten minimalistischen Zeile ist der resultierende Platz für den einzugebenden Befehl.

<img class="fb-img" src="http://cdn.hermann-mayer.net/jity/blog/3/shell_5.png" />
####Zusätzliche Helfer

Zum zusätzlichen Funktionsumfang gehört eine kleine Sammlung nützlicher Helfer die ich
im Folgenden näher beschreibe und mit einem Beispiel-Aufruf versehe:

<pre class="prettyprintlang-bash linenums"><code># openports - Gibt alle geöffneten TCP Ports aus
[..] $ openports

# mount - Alias zum normalen „mount“, allerdings mit tabellarischer Ausgabe
[..] $ mount

# print256 - Gibt eine Tabelle mit den Farbcodes des 256 Farben Terminals aus
[..] $ print256

# printSystemInfo - Gibt eine Übersicht über den aktuellen Systemstatus aus
[..] $ printSystemInfo

# finder - Durchsucht alle Dateien im Arbeitsverzeichnis (rekursiv) nach der angegeben Zeichenkette
[..] $ finder 'class TagController'

# unixtimetodate - Konvertiert einen Unix-Zeitstempel in ein lesbares Format
[..] $ unixtimetodate 1351285151

# datediff - Gibt die Differenz in Tagen zwischen zwei Daten aus
[..] $ datediff 2012-10-30 2012-10-25

# userinfo - (Für LDAP Nutzer - Konfiguration Vorrausgesetzt) Gibt interessante Informationen (zB. Telefonnummer) des angegeben Nutzers aus
[..] $ userinfo 'h.mayer'

# extract - Transparentes Entpacken aller gängigen Archive
[..] $ extract archiv.xz

# debughtml - Reformatiert und Highlightet einen HTML Stdin Stream
[..] $ curl -s 'http://www.google.de' | debughtml

# debugxml - Reformatiert und Highlightet einen XML Stdin Stream
[..] $ curl -s 'http://www.comptechdoc.org/independent/web/xml/guide/parts.xml' | debugxml</code></pre>

####Installation

Dem willigen Nutzer oder Tester, soll es natürlich so einfach wie möglich gemacht werden,
die Konfiguration zu installieren, darum habe ich eine kleine Installationsanleitung, die aus
drei Schritten besteht, nachstehend verfasst. Wer sich den Code doch lieber zunächst einmal
ansehen möchte, der kann dies gerne [hier](http://gitorious.hermann-mayer.net/linux-environment/vim-config/blobs/master/.bashrc) tun.

<pre class="prettyprintlang-bash linenums"><code># In dein HOME Verzeichnis wechseln
[..] $ cd ~

# Backup deiner alten BashRC anlegen
[..] $ mv .bashrc .bashrc.old

# Abholen der neuen BashRC über einen Direktlink der immer auf die neuste Version Zeigt
[..] $ wget -q 'http://gitorious.hermann-mayer.net/linux-environment/vim-config/blobs/raw/master/.bashrc' -O .bashrc</code></pre>


###Vim als IDE

Wer schon mal mit Eclipse gearbeitet hat wird festgestellt haben, dass es ein sehr mächtiges Werkzeug
ist. Dabei ist Eclipse aber sehr überladen, teils verwirrend strukturiert (gerade in den Menüs und den
Einstellungen) und vor allem in vielen Situationen sehr leistungshungrig, was sich fast immer, auf 
den Workflow, in dieser großen IDE, niederschlägt. Diverse Tricks, wie das Vergrößern von maximal
allokierbaren Arbeitsspeicher, bewirken zum Teil spürbare Verbesserungen, doch zu lasten der
Systemleistung, gerade bei Systemen mit wenig Arbeitsspeicher. Natürlich ist Eclipse nicht die einzige
IDE auf den Markt. Des Weiteren finden sich auch andere Kandidaten, wie Netbeans, oder für
PHP Entwickler auch PHPStorm. Diese Werkzeuge sind alle GUI-basiert, was in einer
Konsolen-Umgebung, wie in einer SSH Sitzung, nutzlos ist. Aus diesem Grund, wurde mein Interesse
an einer Lösung geweckt, welche sich in einem Terminal verwenden ließe. Denn einen Terminal hat
man in aller Regel auf einem entfernten, als auch auf dem lokalen System. Der sich daraus ergebende
Vorteil, ist am Ende die Gewohnheit, nur noch ein Werkzeug zu nutzen. *Aber warum gerade Vim?*

Aus der Intention heraus, ausschließlich Open-Source Software zu nutzen, entschied ich mich für
das althergebrachte Vim. Auch weil es zu den Standard Bordmitteln einer jeden Linux Distribution
gehört und sehr leichtgewichtig und äußerst gut erweiterbar ist. Durch die Möglichkeiten die Vim
„Out-of-the-Box“ mitbringt, ist es auch ein leichtes, diesen programmierbaren Editor, in eine IDE zu
verwandeln. Und so sieht eine Vim Sitzung bei mir aus:
 
<img class="fb-img" src="http://cdn.hermann-mayer.net/jity/blog/3/vim_6.png" /> 

####Was unter der Haube steckt

Wie unschwer zu erkennen ist, kann man alle Features die zB. Eclipse bietet, genauso auf Vim übertragen.
Dazu zählt ein Dateimanager, die Outline (Klassen-, Funktionen- und Variablenauflistung) der
aktuellen Datei, Automatische Vervollständigung von kontextspezifischen Namen (Funktionens-,
Variablen- und Klassennamen), das Syntax Highlighting, sowie die Syntaktische Analyse. 
Des Weiteren kann man, mit eingerichtetem CTags, bequem von einem Methodenaufruf zu dessen
Definition, in einer anderen Datei, spirngen.

Wem aufgefallen ist, dass auf dem Bildschirmfoto gar kein Terminal zu sehen ist, der hat es richtig
erkannt. Denn es gibt ein Vim-Frontend, für die grafische Umgebung, das sich gVim nennt. Datei werden
dem Editor, zusätzliche Schaltflächen, zum speichern und editieren am oberen Ende, hinzugefügt. Ich
nutze diese Zusätze nicht, empfinde sie aber auch nicht als störend. Allgemein lässt sich dabei aber sagen,
dass es keinen Unterschied macht, ob Vim auf der Konsole benutzt wird, oder über gVim mit einem
separaten Fenster. Alle oben genannten Features sind unabhängig davon nutzbar.

<img class="fb-img" src="http://cdn.hermann-mayer.net/jity/blog/3/vim_2.png" /> 

####Beautiful Vim Cheat-Sheet

Als Zusatzinformation, möchte ich hier ein sehr nützliches Kickstarter Projekt erwähnen, welches
sowohl dem Anfänger, als auch dem Erfahrenen Vim Nutzer, behilflich sein kann: Die
„[Beautiful Vim Cheat-Sheet](http://vimcheatsheet.com/)“. Sie stellt, sehr ansprechend
gestaltet, alle wichtigen Vim Tastenbelegungen, Funktionen und Eigenheiten, dar. 

####Übersicht zur Vim Konfiguration

* **Plugins** verwaltet durch Vundle
* **Datei Browser** Durch das NERDTree Plugin
    * F3 &rarr; Blendet den Datei Browser ein oder aus
* **Einfache Fenster Navigation** mittels Alt + Pfeiltasten
* **Class/Outline Browser**
    * F4 &rarr; Blendet den Class/Outline Browser ein oder aus
* **SuperTab** Man kann überall Tab drücken, zB. um Pfade oder Dateien einzufügen
* **Neocomplcache** Sehr nützliche Automatische Vervollständigung
* **Einfache Tab Navigation**
    * tt &rarr; Öffnet einen neuen, leeren Tab
    * tn oder Ctrl-Shift-Rechts &rarr; Nächster Tab
    * tp oder Ctrl-Shift-Left &rarr; Vorheriger Tab
    * tl oder F2 &rarr; Zeigt eine Liste aller Tabs
* **Align** 
<pre class="prettyprintlang-bash linenums"><code>var x = y;
var usefullVarName = 42;
var trickyHacky = x;

# Einfach ':Align =' eingeben und man erhält:

var x              = y;
var usefullVarName = 42;
var trickyHacky    = x;
</code></pre>
* **Auskommentieren von Code** 
    * gcc &rarr; Aktuelle Zeile Ein- oder Auskommentieren
* **Nützliche Komandos auf Shortcuts**
    * F8 &rarr; Alignment von "=" unter dem Cursor 
    * F9 &rarr; Alignment von "=>" unter dem Cursor (PHP Array)
    * F10 &rarr; Entfernt alle ASCII Steuerzeichen (zB. ^M (Windows Zeilenende))
    * F11 &rarr; Syntax Check des gespeicherten Zustandes
    * F12 &rarr; Autogenerierung der CTags anstoßen (Projektpfade müssen konfiguriert werden)

####Installation 

Auch hier soll es dem willigen Nutzer oder Tester so einfach wie möglich gemacht werden,
die Konfiguration zu installieren. Deshalb habe ich hier auch wieder eine kleine
Installationsanleitung, die aus wenigen Schritten besteht, verfasst. Wer sich den
Code des Projektes gern im Vorhinein anschauen möchte, kann dies [hier](https://gitorious.hermann-mayer.net/linux-environment/vim-config) tun.

<pre class="prettyprintlang-bash linenums"><code># In dein HOME Verzeichnis wechseln
[..] $ cd ~

# Hiermit legen wir einen neuen Pfad an und Klonen das Projekt Repository dort hin
[..] $ mkdir -p ~/projects/vim-config
[..] $ git clone 'git://gitorious.hermann-mayer.net/linux-environment/vim-config.git' ~/projects/vim-config

# Anschließend wechseln wir in das geklonte Repository
# und Verknüpfen die darin enthaltenen Dateien in dein HOME Verzeichnis
[..] $ cd ~/projects/vim-config
[..] $ ln -s `pwd`/.* ~

# Abschließend müssen wir Vim zum ersten mal mit der neuen Konfiguration
# öffnen und alle Abhängigkeiten automatisch auflösen lassen
[..] $ vim

# Danach einfach folgendes eingeben:
:BundleInstall!

# Linker Hand ist eine Liste aller Abhängigkeiten zu sehen, die nacheinander abgeholt werden
# Sobald dieser Prozess beendet ist einfach Vim beenden mit:
:q!

# Nun ist Vim erfolgreich eingerichtet und kann ganz normal verwendet werden, mit
# allen Features die ich weiter oben beschrieben habe
</code></pre>

####Abschließende Worte

Ich hoffe, wie immer, dass man etwas aus diesem Artikel mitnehmen konnte. Sollte es
Probleme oder Unklarheiten geben helfe ich natürlich gerne. Auch über Feedback würde
ich mich freuen.  <img src="{{ asset("bundles/jityhomepage/img/emoticons/happy.png") }}" />


*[tar]: Ein im Unix-Umfeld sehr geläufigen Archivierungsprogramm
*[git]: Ist eine freie Software zur verteilten Versionsverwaltung von Dateien, die ursprünglich für die Quellcode-Verwaltung des Linux-Kernels entwickelt wurde
*[svn]: Subversion – Eine freie Software zur Versionsverwaltung von Dateien und Verzeichnissen 
*[oder man]: Ein Programm zum Anzeigen von Seiten des Online-Handbuches
*[grep]: Ein Programm das Zeilen ausgibt die einem Muster entsprechen
*[mkdir]: Ein Programm das Verzeichnisse anlegt
*[more]: Ist ein Befehl auf der Kommandozeile zum Betrachten großer Datenmengen
*[less]: Ist ein unixoides Programm zum Betrachten großer Datenmengen
*[cd]: Auf manchen Plattformen auch chdir (für change directory), ist ein Kommandozeilenbefehl für den Einsatz in Shells, der ein Wechseln des Arbeitsverzeichnisses ermöglicht
*[Command Prompt]: Kommandozeile, Befehlszeile oder aus dem Englischen command-line interface, kurz CLI, oft auch als Konsole oder Terminal bezeichnet
*[Bash RC]: Konfigurationsdatei der Bourne-again shell
*[Automatischen Vervollständigung]: Bash Completion


*[Out-of-the-Box]: Von der Stange, Gebrauchsfertig, direkt Einsatzbereit
*[IDE]: Integrierte Entwicklungsumgebung (Abkürzung IDE, von engl. integrated development environment)
*[Vim]: Vim (Vi IMproved) ist eine Weiterentwicklung des Texteditors vi
*[GUI]: Grafische Benutzeroberfläche, abgekürzt GUI (engl. „Graphical User Interface“) 
