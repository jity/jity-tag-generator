<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Filter;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\Tests\Fixture\PreparedTagCollection,
    Jity\TagGeneratorBundle\Filter\Occurrence;

/**
 * OccurrenceTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class OccurrenceTest extends JityTestCase
{
    private $collection;

    /**
     * setUp
     *
     * @access public
     * @return void
     */
    public function setUp()
    {
        $this->collection = new PreparedTagCollection();
    }

    /**
     * bug
     *
     * @param mixed $tag
     *
     * @access private
     * @return void
     */
    private function bug($tag)
    {
        $this->fail('Error: [' . $tag . '] does not match the occurrence. Seems to be a Bug.');
    }

    /**
     * testSoftProcess
     *
     * @access public
     * @return void
     */
    public function testSoftProcess()
    {
        $filter = new Occurrence(2, false, array(
            'min' => '5',
            'max' => '7'
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('blog' == $tag->getName()) {
                $this->assertEquals('5', $tag->getOccurrence());
                $this->assertEquals('11', $tag->getScore());
            }

            if ('KDE4' == $tag->getName()) {
                $this->assertEquals('6', $tag->getOccurrence());
                $this->assertEquals('13', $tag->getScore());
            }

            if ('business' == $tag->getName()) {
                $this->assertEquals('7', $tag->getOccurrence());
                $this->assertEquals('15', $tag->getScore());
            }
        }
    }

    /**
     * testHardProcess
     *
     * @access public
     * @return void
     */
    public function testHardProcess()
    {
        $filter = new Occurrence(2, true, array(
            'min' => '5',
            'max' => '6'
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('blog' == $tag->getName()) {
                $this->assertEquals('5', $tag->getOccurrence());
                $this->assertEquals('11', $tag->getScore());
            }

            if ('KDE4' == $tag->getName()) {
                $this->assertEquals('6', $tag->getOccurrence());
                $this->assertEquals('13', $tag->getScore());
            }

            if ('jEdit' == $tag->getName()) {
                $this->bug($tag->getName());
            }

            if ('link' == $tag->getName()) {
                $this->bug($tag->getName());
            }
        }
    }
}

