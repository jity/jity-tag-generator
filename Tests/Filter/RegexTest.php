<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Filter;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\Tests\Fixture\PreparedTagCollection,
    Jity\TagGeneratorBundle\Filter\Regex;

/**
 * RegexTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class RegexTest extends JityTestCase
{
    private $collection;

    /**
     * setUp
     *
     * @access public
     * @return void
     */
    public function setUp()
    {
        $this->collection = new PreparedTagCollection();
    }

    /**
     * bug
     *
     * @param mixed $tag
     *
     * @access private
     * @return void
     */
    private function bug($tag)
    {
        $this->fail('Error: [' . $tag . '] does not match regex pattern. Seems to be a Bug.');
    }

    /**
     * testSoftProcess
     *
     * @access public
     * @return void
     */
    public function testSoftProcess()
    {
        $filter = new Regex(2, false, array(
            'pattern'     => '[0-9]$',
            'modificator' => ''
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('KDE4' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('YouTube' == $tag->getName()) {
                $this->assertEquals('1', $tag->getScore());
            }

            if ('jEdit' == $tag->getName()) {
                $this->assertEquals('1', $tag->getScore());
            }
        }
    }

    /**
     * testHardProcess
     *
     * @access public
     * @return void
     */
    public function testHardProcess()
    {
        $filter = new Regex(2, true, array(
            'pattern'     => '^[0-9]+$',
            'modificator' => ''
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('2005' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('1990' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('jEdit' == $tag->getName()) {
                $this->bug($tag->getName());
            }

            if ('link' == $tag->getName()) {
                $this->bug($tag->getName());
            }
        }
    }
}

