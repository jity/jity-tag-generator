<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Filter;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\Tests\Fixture\PreparedTagCollection,
    Jity\TagGeneratorBundle\Filter\ScoreGroup;

/**
 * ScoreGroupTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class ScoreGroupTest extends JityTestCase
{
    private $collection;

    /**
     * setUp
     *
     * @access public
     * @return void
     */
    public function setUp()
    {
        $this->collection = new PreparedTagCollection(true);
    }

    /**
     * bug
     *
     * @param mixed $tag
     *
     * @access private
     * @return void
     */
    private function bug($tag)
    {
        $this->fail('Error: [' . $tag . '] does not match the correct score. Seems to be a Bug.');
    }

    /**
     * testHardProcess
     *
     * @access public
     * @return void
     */
    public function testHardProcess()
    {
        $filter = new ScoreGroup(1, true, array(
            'max' => '2'
        ));

        $result = $filter->process($this->collection);
        $check  = array();

        foreach ($result as $tag) {
            $check[$tag->getScore()][] = $tag;
        }

        $this->assertEquals(2, count($check));
    }
}

