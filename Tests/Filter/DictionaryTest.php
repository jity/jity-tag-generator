<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Filter;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\Tests\Fixture\PreparedTagCollection,
    Jity\TagGeneratorBundle\Filter\Dictionary;


/**
 * DictionaryTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class DictionaryTest extends JityTestCase
{
    private $collection;

    /**
     * setUp
     *
     * @access public
     * @return void
     */
    public function setUp()
    {
        $this->collection = new PreparedTagCollection();
    }

    /**
     * bug
     *
     * @param mixed $tag
     *
     * @access private
     * @return void
     */
    private function bug($tag)
    {
        $this->fail('Error: [' . $tag . '] should not be found in dictionary. Seems to be a Bug.');
    }

    /**
     * testConfigure
     *
     * @access public
     * @return void
     */
    public function testConfigure()
    {
        try {

            $filter = new Dictionary(10, false, array(
                'match'         => true,
                'casesensitive' => false,
                'dictionaries'  => array(
                    'doesNotExist' => array(
                        'nothing'
                    )
                )
            ));

        } catch (\Exception $e) {

            $this->assertEquals('Dictionary "nothing" was not found.', $e->getMessage());
        }
    }

    /**
     * testSoftProcess
     *
     * @access public
     * @return void
     */
    public function testSoftProcess()
    {
        $filter = new Dictionary(10, false, array(
            'match'         => true,
            'casesensitive' => false,
            'dictionaries'  => array(
                'german'    => array(
                    'worte'
                )
            )
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('post' == $tag->getName()) {
                $this->assertEquals('10', $tag->getScore());
            }

            if ('distribution' == $tag->getName()) {
                $this->assertEquals('10', $tag->getScore());
            }

            if ('content' == $tag->getName()) {
                $this->assertEquals('1', $tag->getScore());
            }
        }

        $result[0]->setName('New');
        $this->assertEquals('New', $result[0]->getName());
    }

    /**
     * testHardProcess
     *
     * @access public
     * @return void
     */
    public function testHardProcess()
    {
        $filter = new Dictionary(1, true, array(
            'match'         => false,
            'casesensitive' => false,
            'dictionaries'  => array(
                'german'    => array(
                    'worte'
                )
            )
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('post' == $tag->getName()) {
                $this->assertEquals('1', $tag->getScore());
            }

            if ('distribution' == $tag->getName()) {
                $this->assertEquals('1', $tag->getScore());
            }

            if ('jEdit' == $tag->getName()) {
                $this->bug($tag->getName());
            }
        }
    }
}

