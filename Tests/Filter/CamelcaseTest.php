<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Filter;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\Tests\Fixture\PreparedTagCollection,
    Jity\TagGeneratorBundle\Filter\Camelcase;

/**
 * CamelcaseTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class CamelcaseTest extends JityTestCase
{
    private $collection;

    /**
     * setUp
     *
     * @access public
     * @return void
     */
    public function setUp()
    {
        $this->collection = new PreparedTagCollection();
    }

    /**
     * bug
     *
     * @param mixed $tag
     *
     * @access private
     * @return void
     */
    private function bug($tag)
    {
        $this->fail('Error: [' . $tag . '] is not camelcased. Seems to be a Bug.');
    }

    /**
     * testSoftProcess
     *
     * @access public
     * @return void
     */
    public function testSoftProcess()
    {
        $filter = new Camelcase(2);
        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('YouTube' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('jEdit' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('KDE' == $tag->getName()) {
                $this->assertEquals('1', $tag->getScore());
            }
        }
    }

    /**
     * testHardProcess
     *
     * @access public
     * @return void
     */
    public function testHardProcess()
    {
        $filter = new Camelcase(2, true);
        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('YouTube' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('jEdit' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('KDE' == $tag->getName()) {
                $this->bug($tag->getName());
            }

            if ('link' == $tag->getName()) {
                $this->bug($tag->getName());
            }
        }
    }
}

