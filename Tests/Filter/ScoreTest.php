<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Filter;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\Tests\Fixture\PreparedTagCollection,
    Jity\TagGeneratorBundle\Filter\Score;

/**
 * ScoreTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class ScoreTest extends JityTestCase
{
    private $collection;

    /**
     * setUp
     *
     * @access public
     * @return void
     */
    public function setUp()
    {
        $this->collection = new PreparedTagCollection(true);
    }

    /**
     * bug
     *
     * @param mixed $tag
     *
     * @access private
     * @return void
     */
    private function bug($tag)
    {
        $this->fail('Error: [' . $tag . '] does not match the correct score. Seems to be a Bug.');
    }

    /**
     * testSoftProcess
     *
     * @access public
     * @return void
     */
    public function testSoftProcess()
    {
        $filter = new Score(10, false, array(
            'min' => '1',
            'max' => '10'
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ($tag->getScore() > 15) {

                $origScore = (($tag->getScore() / 10) <= 10) ? true : false;

                if (false === $origScore) {
                    $this->bug($tag->getName() . ' : ' . $tag->getScore());
                }

                $this->assertTrue($origScore);
            }
        }
    }

    /**
     * testHardProcess
     *
     * @access public
     * @return void
     */
    public function testHardProcess()
    {
        $filter = new Score(10, true, array(
            'min' => '1',
            'max' => '10'
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ($tag->getScore() > 15) {

                $origScore = (($tag->getScore() / 10) <= 10) ? true : false;

                if (false === $origScore) {
                    $this->bug($tag->getName() . ' : ' . $tag->getScore());
                }

                $this->assertTrue($origScore);

            } else {

                $this->bug($tag->getName() . ':' . $tag->getScore());
            }
        }
    }
}

