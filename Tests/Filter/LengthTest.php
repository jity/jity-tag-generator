<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Filter;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\Tests\Fixture\PreparedTagCollection,
    Jity\TagGeneratorBundle\Filter\Length;

/**
 * LengthTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class LengthTest extends JityTestCase
{
    private $collection;

    /**
     * setUp
     *
     * @access public
     * @return void
     */
    public function setUp()
    {
        $this->collection = new PreparedTagCollection();
    }

    /**
     * bug
     *
     * @param mixed $tag
     *
     * @access private
     * @return void
     */
    private function bug($tag)
    {
        $this->fail('Error: [' . $tag . '] is not 3-4 chars long. Seems to be a Bug.');
    }

    /**
     * testSoftProcess
     *
     * @access public
     * @return void
     */
    public function testSoftProcess()
    {
        $filter = new Length(2, false, array(
            'min' => '3',
            'max' => '3'
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('web' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('ORM' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('link' == $tag->getName()) {
                $this->assertEquals('1', $tag->getScore());
            }
        }
    }

    /**
     * testHardProcess
     *
     * @access public
     * @return void
     */
    public function testHardProcess()
    {
        $filter = new Length(2, true, array(
            'min' => '3',
            'max' => '4'
        ));

        $result = $filter->process($this->collection);

        foreach ($result as $tag) {

            if ('ORM' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('KDE4' == $tag->getName()) {
                $this->assertEquals('2', $tag->getScore());
            }

            if ('online' == $tag->getName()) {
                $this->bug($tag->getName());
            }

            if ('Google' == $tag->getName()) {
                $this->bug($tag->getName());
            }
        }
    }
}

