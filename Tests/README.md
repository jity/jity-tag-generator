# Running the Jity Testsuite

## Running tests

Before you can begin you need to install Doctrine Common.

    pear install doctrine/DoctrineCommon

Execute PHPUnit in the root folder of your jity/tag-generator clone.

    phpunit

