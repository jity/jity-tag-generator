<?php

/*
 * This file bootstraps the test environment.
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests;

error_reporting(E_ALL | E_STRICT);

// register silently failing autoloader
spl_autoload_register(function($class)
{
    if (0 === strpos($class, 'Jity\TagGeneratorBundle\\')) {

        $path = __DIR__.'/../../../'.($class = strtr($class, '\\', '/')).'.php';

        if (false === file_exists($path) || false === is_readable($path)) {

            $path = __DIR__ . '/../'
                . str_replace('Jity/TagGeneratorBundle/', '', strtr($class, '\\', '/'))
                . '.php';
        }

        require_once $path;

        return true;

    } else if (0 === strpos($class, 'Doctrine\Common\\')) {

        if ('' === PEAR_INSTALL_DIR) {

            echo 'Error: Your PEAR seems not to work correctly.' . PHP_EOL
                 . 'PEAR_INSTALL_DIR is undefined.';
            exit;
        }

        $path = PEAR_INSTALL_DIR . '/' . ($class = strtr($class, '\\', '/')).'.php';

        require_once $path;

        return true;
    }
});

