<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\DependencyInjection\TagGenerator,
    Jity\TagGeneratorBundle\Entity\TagCollection,
    Jity\TagGeneratorBundle\Filter\Score,
    Jity\TagGeneratorBundle\Filter\ScoreGroup,
    Jity\TagGeneratorBundle\Filter\Length,
    Jity\TagGeneratorBundle\Filter\Occurrence,
    Jity\TagGeneratorBundle\Filter\Dictionary,
    Jity\TagGeneratorBundle\Filter\Capitalized,
    Jity\TagGeneratorBundle\Filter\Uppercase,
    Jity\TagGeneratorBundle\Filter\Camelcase,
    Jity\TagGeneratorBundle\Filter\Regex,
    Jity\TagGeneratorBundle\Formater\SimpleFormater,
    Jity\TagGeneratorBundle\Splitter\SimpleSplitter;

/**
 * TagGeneratorTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class TagGeneratorTest extends JityTestCase
{
    private $generator;

    /**
     * setUp
     *
     * @access public
     * @return void
     */
    public function setUp()
    {
        // Instantiate a new Generator
        $generator = new TagGenerator();

        // Configure all Filters
        $generator

            /* Remove words shorter than 3 chars */
            ->addFilter(
                new Length(1, true, array(
                    'min' => 2
                ))
            )

            /* Remove most useless words from collection */
            ->addFilter(
                new Dictionary(1, true, array(
                    'match'         => true,
                    'casesensitive' => false,
                    'dictionaries'  => array(
                        'german'    => array(
                            'adjektive',
                            'verben',
                            'klein',
                            'fixwords',
                            'stopwords'
                        )
                    )
                ))
            )

            /* Score words ackknowledged by our dictionary */
            ->addFilter(
                new Dictionary(5, false, array(
                    'match'         => true,
                    'casesensitive' => false,
                    'dictionaries'  => array(
                        'german'    => array(
                            'worte'
                        )
                    )
                ))
            )

            /* Score occurrence of remaining words */
            ->addFilter(
                new Occurrence(5)
            )

            /* Score uppercased words */
            ->addFilter(new Uppercase(15))

            /* Score camelcased words */
            ->addFilter(new Camelcase(15))

            /* Score capitalized words */
            ->addFilter(new Capitalized(5))

            ->addFilter(
                new Regex(2, false, array(
                    'pattern' => '[-]+'
                ))
            )

            /* Score words which are not ackknowledged by our dictionary */
            ->addFilter(
                new Dictionary(10, false, array(
                    'match'         => false,
                    'casesensitive' => false,
                    'dictionaries'  => array(
                        'german'    => array(
                            'worte'
                        )
                    )
                ))
            )

            /* Remove words which does not fit the range */
            ->addFilter(
                new Score(1, true, array(
                    'min' => 300
                ))
            )

            /* Remove words by score-groups */
            ->addFilter(
                new ScoreGroup(1, true, array(
                    'max' => 3
                ))
            );

        $this->generator = $generator;
    }

    /**
     * testGenerator
     *
     * @access public
     * @return void
     */
    public function testGenerator()
    {
        $formater = new SimpleFormater();
        $splitter = new SimpleSplitter();

        $generator = new TagGenerator();
        $generator
            ->setFormater($formater)
            ->setSplitter($splitter);

        $collection = new TagCollection(array(
            'tag' => 1
        ));

        $tags = $generator->filter($collection);

        $this->assertEquals($tags, $collection);
    }

    /**
     * testIntegration
     *
     * @access public
     * @return void
     */
    public function testIntegration()
    {
        $start = microtime(true);
        $count = 0;
        echo "\n";

        foreach (glob(__DIR__ . '/../Fixture/Resources/*') as $file) {

            $text   = file_get_contents($file);
            $debug  = array();
            $output = array();
            $count++;

            // Receive the collection of tags
            $tags = $this->generator->getTags($text);

            $this->assertEquals($text, $tags->getSource());

            foreach ($tags as $tag) {
                $output[] = $tag->getName();
                $debug[] = $tag->getScore() . ' - ' . $tag->getName();
            }

            $this->assertTrue(true);

            krsort($debug);
            krsort($output);

            if ('Bash als Power-Shell und Vim als IDE.md' === basename($file)) {

                if (false === in_array('IDE', $output)) {
                    $this->fail('Tag [IDE] not found in ' . $file . '.');
                }
            }

            if ('Blindtexte-wiki.txt' === basename($file)) {

                if (false === in_array('Blindtext', $output)) {
                    $this->fail('Tag [Blindtext] not found in ' . $file . '.');
                }
            }

            if ('Doctrine - Many-to-Many - Manuelles Ordnen der Beziehung.md' === basename($file)) {

                if (false === in_array('ORM', $output)) {
                    $this->fail('Tag [ORM] not found in ' . $file . '.');
                }
            }

            if ('Firefox 17 - Scroll-Geschwindigkeit.md' === basename($file)) {

                if (false === in_array('Firefox', $output)) {
                    $this->fail('Tag [Firefox] not found in ' . $file . '.');
                }
            }
        }

        // Test the useless path of the filter
        $scoreGroup = new ScoreGroup(1, false);
        $this->assertEquals($tags, $scoreGroup->process($tags));

        $time = (microtime(true) - $start);
        echo "\n[Stat: Integration Test took " . number_format($time, 15) . 's for parsing ' . $count .  " files]\n";
    }
}

