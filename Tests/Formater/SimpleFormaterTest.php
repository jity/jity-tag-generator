<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Formater;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\Formater\SimpleFormater;

/**
 * SimpleFormaterTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class SimpleFormaterTest extends JityTestCase
{
    /**
     * testProcess
     *
     * @access public
     * @return void
     */
    public function testProcess()
    {
        $formater = new SimpleFormater();

        $this->assertEquals(
            'This is a simple-test-case',
            $formater->process('This is a simple-test-case?!')
        );

        $this->assertEquals(
            'äÜö',
            $formater->process('~äÜö#')
        );

        $this->assertEquals(
            '1992-08-02 User',
            $formater->process('1992-08-02|User')
        );

        try {

            $this->assertEquals(
                array('Word', 'Word', 'Test', 'Test', 'Test', 'Abc'),
                $formater->process(array('Word', 'Word', 'Test', 'Test', 'Test', 'Abc'))
            );

        } catch (\Exception $e) {

            $this->assertEquals(
                'Jity\TagGeneratorBundle\Formater\SimpleFormater->process(string) :: Param needs to be a string',
                $e->getMessage()
            );
        }
    }
}

