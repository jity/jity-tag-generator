<?php

/*
 * This file is part of the Jity package.
 *
 * (c) Hermann Mayer <hermann.mayer92@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jity\TagGeneratorBundle\Tests\Splitter;

use Jity\TagGeneratorBundle\Tests\JityTestCase,
    Jity\TagGeneratorBundle\Splitter\SimpleSplitter;

/**
 * SimpleSplitterTest
 *
 * @uses JityTestCase
 * @author Hermann Mayer <hermann.mayer92@gmail.com>
 */
class SimpleSplitterTest extends JityTestCase
{
    /**
     * testProcess
     *
     * @access public
     * @return void
     */
    public function testProcess()
    {
        $splitter = new SimpleSplitter();

        $this->assertEquals(
            array(
                'Word' => 2,
                'Test' => 3,
                'Abc' => 1
            ),
            $splitter->process('Word Word Test Test Test Abc')
        );

        try {

            $this->assertEquals(
                array('Word', 'Word', 'Test', 'Test', 'Test', 'Abc'),
                $splitter->process(array('Word', 'Word', 'Test', 'Test', 'Test', 'Abc'))
            );

        } catch (\Exception $e) {

            $this->assertEquals(
                'Jity\TagGeneratorBundle\Splitter\SimpleSplitter->process(string) :: Param needs to be a string',
                $e->getMessage()
            );
        }
    }
}

