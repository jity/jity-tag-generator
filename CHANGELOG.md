# Changelog

## v0.2.0

* Projct is now a Symfony2 Bundle

## v0.1.1

* Testcases written for all filters, formaters and splitters
* Integration testcase move and optimized
* Fixed serval bugs in filters, its good to have unittests :)

## v0.1.0

* Generated a API documentation
* Big cleanup: All files were cleaned up (Documentation, Line-width, etc)

